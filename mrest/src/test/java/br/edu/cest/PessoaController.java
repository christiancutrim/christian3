package br.edu.cest;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PessoaController {
	
	private final PessoaRepository repository;
	
	PessoaController(PessoaRepository repository){
		this.repository = repository;
	}
	
	@GetMapping("/pessoas")
	List<Pessoa> getAll() {
		return repository.findAll();
	}

	
}
