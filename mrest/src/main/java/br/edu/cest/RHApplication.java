package br.edu.cest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages= {"br.edu.cest"})
public class RHApplication {

	public static void main(String[] args) {
		SpringApplication.run(RHApplication.class, args);
	}

}
